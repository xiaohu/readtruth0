# Herwig7
#run_readtruth job-hh4b-kl-m05-hw7 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_HW7/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-m05-hw7_* hh4b
#run_readtruth job-hh4b-kl-p00-hw7 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_HW7/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p00-hw7_* hh4b
#run_readtruth job-hh4b-kl-p01-hw7 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_HW7/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p01-hw7_* hh4b
#run_readtruth job-hh4b-kl-p02p5-hw7 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_HW7/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p02p5-hw7_* hh4b
#run_readtruth job-hh4b-kl-p05-hw7 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_HW7/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p05-hw7_* hh4b
#run_readtruth job-hh4b-kl-p10-hw7 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_HW7/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p10-hw7_* hh4b

# Pythia8
run_readtruth job-hh4b-kl-m05-py8 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_PY8/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-m05-py8_* hh4b
run_readtruth job-hh4b-kl-p00-py8 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_PY8/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p00-py8_* hh4b
run_readtruth job-hh4b-kl-p01-py8 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_PY8/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p01-py8_* hh4b
run_readtruth job-hh4b-kl-p02p5-py8 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_PY8/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p02p5-py8_* hh4b
run_readtruth job-hh4b-kl-p05-py8 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_PY8/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p05-py8_* hh4b
run_readtruth job-hh4b-kl-p10-py8 /afs/cern.ch/work/x/xiaohu/MC/EVGEN/PH_NLOFT_PY8/ DAOD_TRUTH0.bbbb.DAOD.test.*.evgen.root.kl-p10-py8_* hh4b
