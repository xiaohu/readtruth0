**2019-10-14 MOVE TO https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/hh_mc_truth0**

# ReadTRUTH0
This is a package to read truth information in the TRUTH0 format being an output of Generate_tf.py in Athena

Look at do.sh for examples.

## For the first time
1. setupATLAS
2. lsetup 'rcsetup Base,2.4.28’
3. rc find_packages
4. rc compile

## For every time after
1. setupATLAS
2. source rcSetup.sh