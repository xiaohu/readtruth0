#include <mytruth/readtruth_azheavyh.h>
#include <mytruth/readtruth_hhbbzz4l.h>
#include <mytruth/readtruth_hhbbzzllvv.h>
#include <mytruth/readtruth_hh4b.h>

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#endif

#ifdef __CINT__
#pragma link C++ class readtruth_azheavyh+;
#pragma link C++ class readtruth_hhbbzz4l+;
#pragma link C++ class readtruth_hhbbzzllvv+;
#pragma link C++ class readtruth_hh4b+;
#endif
